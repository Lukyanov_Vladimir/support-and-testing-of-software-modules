package task2.triangleView;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void determineTypeTriangle() {
        Triangle triangle = new Triangle(5, 5, 20);
        Triangle triangle2 = new Triangle(5, 20, 5);
        Triangle triangle3 = new Triangle(20, 5, 5);
        Triangle triangle4 = new Triangle(0, 0, 0);
        Triangle triangle5 = new Triangle(3, 3, 3);
        Triangle triangle6 = new Triangle(2, 3, 3);
        Triangle triangle7 = new Triangle(3, 2, 3);
        Triangle triangle8 = new Triangle(3, 3, 2);
        Triangle triangle9 = new Triangle(6, 5, 3);
        Triangle triangle10 = new Triangle(4, 5, 3);

        assertEquals("Такой треугольник не существует, проверьте данные", triangle.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle2.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle3.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle4.determineTypeTriangle());
        assertEquals("Треугольник является равносторонним", triangle5.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle6.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle7.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle8.determineTypeTriangle());
        assertEquals("Треугольник является неравносторонним", triangle9.determineTypeTriangle());
        assertEquals("Треугольник является прямоугольным", triangle10.determineTypeTriangle());
    }

    @Test
    public void checkExistenceTriangle() {
        Triangle triangle = new Triangle(5, 5, 20);
        Triangle triangle2 = new Triangle(5, 20, 5);
        Triangle triangle3 = new Triangle(20, 5, 5);
        Triangle triangle4 = new Triangle(3, 3, 3);
        Triangle triangle5 = new Triangle(2, 3, 3);
        Triangle triangle6 = new Triangle(3, 2, 3);
        Triangle triangle7 = new Triangle(3, 3, 2);
        Triangle triangle8 = new Triangle(6, 5, 3);
        Triangle triangle9 = new Triangle(4, 5, 3);

        assertFalse(triangle.isExistenceTriangle());
        assertFalse(triangle2.isExistenceTriangle());
        assertFalse(triangle3.isExistenceTriangle());
        assertTrue(triangle4.isExistenceTriangle());
        assertTrue(triangle5.isExistenceTriangle());
        assertTrue(triangle6.isExistenceTriangle());
        assertTrue(triangle7.isExistenceTriangle());
        assertTrue(triangle8.isExistenceTriangle());
        assertTrue(triangle9.isExistenceTriangle());
    }

    @Test
    public void checkIfEquilateralTriangle() {
        Triangle triangle = new Triangle(3, 3, 3);

        assertTrue(triangle.isIfEquilateralTriangle());
    }

    @Test
    public void checkWhetherAnIsoscelesTriangle() {
        Triangle triangle = new Triangle(2, 3, 3);
        Triangle triangle2 = new Triangle(3, 2, 3);
        Triangle triangle3 = new Triangle(3, 3, 2);

        assertTrue(triangle.isWhetherAnIsoscelesTriangle());
        assertTrue(triangle2.isWhetherAnIsoscelesTriangle());
        assertTrue(triangle3.isWhetherAnIsoscelesTriangle());
    }

    @Test
    public void checkRightTriangle() {
        Triangle triangle = new Triangle(4, 5, 3);

        assertTrue(triangle.isRightTriangle());
    }
}