package task2.quadrangleView;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuadrangleTest {

    @Test
    public void checkSquare() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(2, 0);
        tops[1] = new Point2D(2, 2);
        tops[2] = new Point2D(4, 2);
        tops[3] = new Point2D(4, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это квадрат", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkRectangle() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(0, 0);
        tops[1] = new Point2D(0, 2);
        tops[2] = new Point2D(4, 2);
        tops[3] = new Point2D(4, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это прямоугольник", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkParallelogram() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(0, 0);
        tops[1] = new Point2D(1, 2);
        tops[2] = new Point2D(4, 2);
        tops[3] = new Point2D(3, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это параллелограмм", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkRhombus() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(1, 0);
        tops[1] = new Point2D(0, 2);
        tops[2] = new Point2D(1, 4);
        tops[3] = new Point2D(2, 2);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это ромб", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkIsoTrapezoid() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(0, 0);
        tops[1] = new Point2D(1, 2);
        tops[2] = new Point2D(3, 2);
        tops[3] = new Point2D(4, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это равнобедренная трапеция", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkGenTrapezoid() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(0, 0);
        tops[1] = new Point2D(2, 2);
        tops[2] = new Point2D(4, 2);
        tops[3] = new Point2D(5, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это трапеция общего вида", quadrangle.initTypeQuadrangle());
    }

    @Test
    public void checkRectTrapezoid() {
        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(2, 0);
        tops[1] = new Point2D(2, 2);
        tops[2] = new Point2D(4, 2);
        tops[3] = new Point2D(5, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        assertEquals("Это прямоугольная трапеция", quadrangle.initTypeQuadrangle());
    }
}