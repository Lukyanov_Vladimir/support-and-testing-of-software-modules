package task2.quadraticEquation;

import org.junit.Test;
import task1.quadrur.NegativeDiscriminantException;
import task1.quadrur.QuadraticEquation;

import static org.junit.Assert.*;

public class QuadraticEquationTest {
    @Test
    public void discriminantIsZero() throws task1.quadrur.NegativeDiscriminantException {
        task1.quadrur.QuadraticEquation quadraticEquation = new task1.quadrur.QuadraticEquation(1, -6, 9);
        double[] expectedArray = {3, 3};
        double[] resultArray = quadraticEquation.findTheRoots();
        assertArrayEquals(expectedArray, resultArray, 0);
    }

    @Test
    public void discriminantIsGreaterZero() throws task1.quadrur.NegativeDiscriminantException {
        task1.quadrur.QuadraticEquation quadraticEquation = new task1.quadrur.QuadraticEquation(1, 17, -18);
        double[] expectedArray = {1, -18};
        double[] resultArray = quadraticEquation.findTheRoots();
        assertArrayEquals(expectedArray, resultArray, 0);
    }

    @Test(expected = task1.quadrur.NegativeDiscriminantException.class)
    public void discriminantLessZero() throws NegativeDiscriminantException {
        task1.quadrur.QuadraticEquation quadraticEquation = new task1.quadrur.QuadraticEquation(5, 3, 7);
        quadraticEquation.findTheRoots();
    }

    @Test
    public void calcRootWithPositiveSignNum() {
        task1.quadrur.QuadraticEquation quadraticEquation = new task1.quadrur.QuadraticEquation(1, -6, 9);
        task1.quadrur.QuadraticEquation quadraticEquation2 = new task1.quadrur.QuadraticEquation(1, 17, -18);

        assertEquals(3, quadraticEquation.calcRootWithPositiveSignNum(quadraticEquation.calcDiscriminant()), 0);
        assertEquals(1, quadraticEquation2.calcRootWithPositiveSignNum(quadraticEquation2.calcDiscriminant()), 0);
    }

    @Test
    public void calcRootWithNegativeSignNum() {
        task1.quadrur.QuadraticEquation quadraticEquation = new task1.quadrur.QuadraticEquation(1, -6, 9);
        task1.quadrur.QuadraticEquation quadraticEquation2 = new QuadraticEquation(1, 17, -18);

        assertEquals(3, quadraticEquation.calcRootWithNegativeSignNum(quadraticEquation.calcDiscriminant()), 0);
        assertEquals(-18, quadraticEquation2.calcRootWithNegativeSignNum(quadraticEquation2.calcDiscriminant()), 0);
    }
}