package task2.sizeShadow;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SizeShadowTest {

    @Test
    public void shadowLengthCount() {
        Snippet snippet1 = new Snippet(new Point2D(6, 10), new Point2D(7, 10));
        Snippet snippet2 = new Snippet(new Point2D(5, 9), new Point2D(10, 10));
        Snippet snippet3 = new Snippet(new Point2D(14, 9), new Point2D(17, 9));
        Snippet snippet4 = new Snippet(new Point2D(8, 8), new Point2D(13, 8));
        Snippet snippet5 = new Snippet(new Point2D(7, 7), new Point2D(15, 7));
        Snippet snippet6 = new Snippet(new Point2D(16, 7), new Point2D(19, 7));

        List<Snippet> snippets = new ArrayList<>();
        snippets.add(snippet1);
        snippets.add(snippet2);
        snippets.add(snippet3);
        snippets.add(snippet4);
        snippets.add(snippet5);
        snippets.add(snippet6);

        SizeShadow sizeShadow = new SizeShadow();
        double result = sizeShadow.calcShadow(snippets);

        assertEquals(21, result, 1);
    }

    @Test
    public void segmentMergeCheck() {
        Snippet snippet1 = new Snippet(new Point2D(5, 9), new Point2D(10, 10));
        Snippet snippet2 = new Snippet(new Point2D(8, 8), new Point2D(13, 8));

        SizeShadow sizeShadow = new SizeShadow();
        assertTrue(sizeShadow.isMerge(snippet1, snippet2));
    }

    @Test
    public void deletionCheck() {
        Snippet snippet1 = new Snippet(new Point2D(5, 9), new Point2D(10, 9));
        Snippet snippet2 = new Snippet(new Point2D(6, 10), new Point2D(7, 10));

        SizeShadow sizeShadow = new SizeShadow();
        assertTrue(sizeShadow.isRemoveSnippet(snippet1, snippet2));
    }
}