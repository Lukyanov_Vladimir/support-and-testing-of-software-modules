package task1.triangle;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void determineTypeTriangle() {
        Triangle triangle = new Triangle(5, 5, 20);
        Triangle triangle2 = new Triangle(5, 20, 5);
        Triangle triangle3 = new Triangle(20, 5, 5);
        Triangle triangle4 = new Triangle(0, 0, 0);
        Triangle triangle5 = new Triangle(3, 3, 3);
        Triangle triangle6 = new Triangle(2, 3, 3);
        Triangle triangle7 = new Triangle(3, 2, 3);
        Triangle triangle8 = new Triangle(3, 3, 2);
        Triangle triangle9 = new Triangle(4, 5, 3);

        assertEquals("Такой треугольник не существует, проверьте данные", triangle.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle2.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle3.determineTypeTriangle());
        assertEquals("Такой треугольник не существует, проверьте данные", triangle4.determineTypeTriangle());
        assertEquals("Треугольник является равносторонним", triangle5.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle6.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle7.determineTypeTriangle());
        assertEquals("Треугольник является равнобедренным", triangle8.determineTypeTriangle());
        assertEquals("Треугольник является неравносторонним", triangle9.determineTypeTriangle());
    }

    @Test
    public void checkExistenceTriangle() {
        Triangle triangle = new Triangle(5, 5, 20);
        Triangle triangle2 = new Triangle(5, 20, 5);
        Triangle triangle3 = new Triangle(20, 5, 5);
        Triangle triangle4 = new Triangle(3, 3, 3);
        Triangle triangle5 = new Triangle(2, 3, 3);
        Triangle triangle6 = new Triangle(3, 2, 3);
        Triangle triangle7 = new Triangle(3, 3, 2);
        Triangle triangle8 = new Triangle(4, 5, 3);

        assertFalse(triangle.checkExistenceTriangle());
        assertFalse(triangle2.checkExistenceTriangle());
        assertFalse(triangle3.checkExistenceTriangle());
        assertTrue(triangle4.checkExistenceTriangle());
        assertTrue(triangle5.checkExistenceTriangle());
        assertTrue(triangle6.checkExistenceTriangle());
        assertTrue(triangle7.checkExistenceTriangle());
        assertTrue(triangle8.checkExistenceTriangle());
    }

    @Test
    public void checkIfEquilateralTriangle() {
        Triangle triangle = new Triangle(3, 3, 3);

        assertTrue(triangle.checkIfEquilateralTriangle());
    }

    @Test
    public void checkWhetherAnIsoscelesTriangle() {
        Triangle triangle = new Triangle(2, 3, 3);
        Triangle triangle2 = new Triangle(3, 2, 3);
        Triangle triangle3 = new Triangle(3, 3, 2);

        assertTrue(triangle.checkWhetherAnIsoscelesTriangle());
        assertTrue(triangle2.checkWhetherAnIsoscelesTriangle());
        assertTrue(triangle3.checkWhetherAnIsoscelesTriangle());
    }
}