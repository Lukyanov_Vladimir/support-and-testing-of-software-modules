package task3;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinearAlgTest {

    @Test
    public void cos() {
        LinearAlg linearAlg = new LinearAlg(4, 4, 4);
        assertEquals(0.40808206181339196, linearAlg.cos(20), 1);
    }

    @Test
    public void pow() {
        LinearAlg linearAlg = new LinearAlg(4, 4, 4);
        assertEquals(4, linearAlg.pow(2,2), 1);
    }

    @Test
    public void solve() {
        LinearAlg linearAlg = new LinearAlg(4, 4, 4);
        assertEquals(139.1067515514044, linearAlg.solve(), 1);
    }
}