package task2.quadraticEquation;

class QuadraticEquation {
    private final double firstRealNumber;
    private final double secondRealNumber;
    private final double thirdRealNumber;

    public QuadraticEquation(double firstRealNumber, double secondRealNumber, double thirdRealNumber) {
        this.firstRealNumber = firstRealNumber;
        this.secondRealNumber = secondRealNumber;
        this.thirdRealNumber = thirdRealNumber;
    }

    public double[] findTheRoots() throws NegativeDiscriminantException {
        double discriminant = calcDiscriminant();
        double[] roots = new double[2];
        if (discriminant == 0) {
            roots[0] = calcRootWithPositiveSignNum(discriminant);
            roots[1] = calcRootWithNegativeSignNum(discriminant);
        } else if (discriminant > 0) {
            roots[0] = calcRootWithPositiveSignNum(discriminant);
            roots[1] = calcRootWithNegativeSignNum(discriminant);
        } else {
            throw new NegativeDiscriminantException("дискриминант меньше нуля");
        }

        return roots;
    }

    public double calcDiscriminant() {
        return Math.pow(secondRealNumber, 2) - (4 * firstRealNumber * thirdRealNumber);
    }

    public double calcRootWithPositiveSignNum(double discriminant) {
        return (-secondRealNumber + Math.sqrt(discriminant)) / (2 * firstRealNumber);
    }

    public double calcRootWithNegativeSignNum(double discriminant) {
        return (-secondRealNumber - Math.sqrt(discriminant)) / (2 * firstRealNumber);
    }
}
