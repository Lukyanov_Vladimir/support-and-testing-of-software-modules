package task2.quadraticEquation;

public class NegativeDiscriminantException extends Exception {
    public NegativeDiscriminantException(String message) {
        super(message);
    }
}
