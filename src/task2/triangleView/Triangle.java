package task2.triangleView;

public class Triangle {
    private final int A;
    private final int B;
    private final int C;

    public Triangle(int firstSideLength, int secondSideLength, int thirdSideLength) {
        A = firstSideLength;
        B = secondSideLength;
        C = thirdSideLength;
    }

    public String determineTypeTriangle() {
        String message;
        if (!isExistenceTriangle()) {
            message = "Такой треугольник не существует, проверьте данные";

        } else if (isIfEquilateralTriangle()) {
            message = "Треугольник является равносторонним";

        } else if (isRightTriangle()) {
            message = "Треугольник является прямоугольным";

        } else if (isWhetherAnIsoscelesTriangle()) {
            message = "Треугольник является равнобедренным";

        } else {
            message = "Треугольник является неравносторонним";
        }

        return message;
    }

    public boolean isExistenceTriangle() {
        return (A + B > C)
                && (A + C > B)
                && (B + C > A);
    }

    public boolean isIfEquilateralTriangle() {
        return A == B && A == C;
    }

    public boolean isWhetherAnIsoscelesTriangle() {
        return A == B || A == C || B == C;

    }

    public boolean isRightTriangle() {
        return C == Math.sqrt(B * B + A * A) || A == Math.sqrt(C * C + B * B) || B == Math.sqrt(C * C + A * A);
    }
}
