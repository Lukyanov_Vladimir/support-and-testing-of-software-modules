package task2.triangleView;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);

            System.out.println("Введите длину первой стороны треуголника");
            int firstSideLength = sc.nextInt();

            System.out.println("Введите длину второй стороны треуголника");
            int secondSideLength = sc.nextInt();

            System.out.println("Введите длину третьей стороны треуголника");
            int thirdSideLength = sc.nextInt();

            Triangle triangle = new Triangle(firstSideLength, secondSideLength, thirdSideLength);

            System.out.println(triangle.determineTypeTriangle());

        } catch (InputMismatchException e) {
            System.err.println("Введены некорректные данные");
            e.printStackTrace();
        }
    }
}
