package task2.quadrangleView;

public class Main {
    public static void main(String[] args) {

        Point2D[] tops = new Point2D[4];
        tops[0] = new Point2D(2 , 0);
        tops[1] = new Point2D(3, 1);
        tops[2] = new Point2D(3, 4);
        tops[3] = new Point2D(5, 0);

        Quadrangle quadrangle = new Quadrangle(tops);
        System.out.println(quadrangle.initTypeQuadrangle());
    }
}
