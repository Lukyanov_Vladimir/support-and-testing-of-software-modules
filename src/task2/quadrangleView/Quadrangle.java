package task2.quadrangleView;

public class Quadrangle {
    private Point2D[] tops;

    private Side leftSide, upSide, rightSide, downSide;

    private double[] angles;

    public Quadrangle(Point2D[] tops) {
        this.tops = tops;
    }

    public String initTypeQuadrangle() {
        String message = "Это прямоугольник общего вида";

        createSides();
        searchAngles();

        if (isSquare()) {
            message = "Это квадрат";
        }

        if (isRhombus()) {
            message = "Это ромб";
        }

        if (isRectangle()) {
            message = "Это прямоугольник";
        }

        if (isParallelogram()) {
            message = "Это параллелограмм";
        }

        if (isGenTrapezoid()) {
            message = "Это трапеция общего вида";
        }

        if (isIsoTrapezoid()) {
            message = "Это равнобедренная трапеция";
        }

        if (isRectTrapezoid()) {
            message = "Это прямоугольная трапеция";
        }

        return message;
    }

    private void createSides() {
        leftSide = new Side(tops[0], tops[1]);
        upSide = new Side(tops[1], tops[2]);
        rightSide = new Side(tops[2], tops[3]);
        downSide = new Side(tops[0], tops[3]);
    }

    private void searchAngles() {
        angles = new double[4];
        angles[0] = cos(downSide, leftSide);
        angles[1] = cos(leftSide, upSide);
        angles[2] = cos(upSide, rightSide);
        angles[3] = cos(rightSide, downSide);
    }

    public double cos(Side side1, Side side2) {
        Point2D vector1 = new Point2D(side1.getPoint2().x - side1.getPoint1().x, side1.getPoint2().y - side1.getPoint1().y);
        Point2D vector2 = new Point2D(side2.getPoint2().x - side2.getPoint1().x, side2.getPoint2().y - side2.getPoint1().y);

        double scalarProduct = vector1.x * vector2.x + vector1.y * vector2.y;

        double lengthVector1 = Math.sqrt(vector1.x * vector1.x + vector1.y * vector1.y);
        double lengthVector2 = Math.sqrt(vector2.x * vector2.x + vector2.y * vector2.y);

        return scalarProduct / (lengthVector1 * lengthVector2);
    }

    private boolean isSquare() {
        return leftSide.getLength() == upSide.getLength() && upSide.getLength() == rightSide.getLength() && rightSide.getLength() == downSide.getLength() &&
                angles[0] == 0 && angles[1] == 0 && angles[2] == 0 && angles[3] == 0;
    }

    private boolean isRectangle() {
        return upSide.getLength() == downSide.getLength() && leftSide.getLength() == rightSide.getLength() &&
                upSide.getLength() != leftSide.getLength() && upSide.getLength() != rightSide.getLength() && downSide.getLength() != leftSide.getLength() && downSide.getLength() != rightSide.getLength() &&
                angles[0] == 0 && angles[1] == 0 && angles[2] == 0 && angles[3] == 0;
    }

    private boolean isParallelogram() {
        return upSide.getLength() == downSide.getLength() && leftSide.getLength() == rightSide.getLength() &&
                upSide.getLength() != leftSide.getLength() && upSide.getLength() != rightSide.getLength() && downSide.getLength() != leftSide.getLength() && downSide.getLength() != rightSide.getLength() &&
                angles[0] != 0 && angles[1] != 0 && angles[2] != 0 && angles[3] != 0;
    }

    private boolean isRhombus() {
        return leftSide.getLength() == upSide.getLength() && upSide.getLength() == rightSide.getLength() && rightSide.getLength() == downSide.getLength() &&
                angles[0] != 0 && angles[1] != 0 && angles[2] != 0 && angles[3] != 0;
    }

    private boolean isIsoTrapezoid() {
        return leftSide.getLength() == rightSide.getLength() && upSide.getLength() != downSide.getLength() &&
                angles[0] != 0 && angles[1] != 0 && angles[2] != 0 && angles[3] != 0;
    }

    private boolean isGenTrapezoid() {
        return leftSide.getLength() != upSide.getLength() && leftSide.getLength() != rightSide.getLength() && leftSide.getLength() != downSide.getLength() &&
                upSide.getLength() != rightSide.getLength() && upSide.getLength() != downSide.getLength() &&
                rightSide.getLength() != downSide.getLength() &&
                angles[0] != 0 && angles[1] != 0 && angles[2] != 0 && angles[3] != 0;
    }

    private boolean isRectTrapezoid() {
        return (angles[0] == 0 && angles[1] == 0 && angles[2] != 0 && angles[3] != 0) ||
                (angles[1] == 0 && angles[2] == 0 && angles[0] != 0 && angles[3] != 0) ||
                (angles[1] == 0 && angles[2] == 0 && angles[0] != 0 && angles[3] != 0) ||
                (angles[2] == 0 && angles[3] == 0 && angles[0] != 0 && angles[1] != 0) ||
                (angles[3] == 0 && angles[0] == 0 && angles[1] != 0 && angles[2] != 0);
    }
}
