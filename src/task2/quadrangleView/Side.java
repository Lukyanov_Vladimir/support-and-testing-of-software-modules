package task2.quadrangleView;

public class Side {
    private Point2D point1;
    private Point2D point2;

    private double length;

    public Side(Point2D point1, Point2D point2) {
        this.point1 = point1;
        this.point2 = point2;

        calcLength();
    }

    public Point2D getPoint1() {
        return point1;
    }

    public Point2D getPoint2() {
        return point2;
    }

    public double getLength() {
        return length;
    }

    private void calcLength() {
        double dx = point2.x - point1.x;
        double dy = point2.y - point1.y;

        length = Math.sqrt(dx * dx + dy * dy);
    }
}
