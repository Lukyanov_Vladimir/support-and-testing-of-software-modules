package task2.sizeShadow;

public class Snippet {
    private Point2D point1;
    private Point2D point2;

    public Snippet(Point2D point1, Point2D point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public Point2D getPoint1() {
        return point1;
    }

    public Point2D getPoint2() {
        return point2;
    }

    public void setPoints(Point2D point_1, Point2D point_2) {
        this.point1 = point_1;
        this.point2 = point_2;
    }

    public double calcLength() {
        double dx = point2.x - point1.x;
        double dy = point2.y - point1.y;

        return Math.sqrt(dx * dx + dy * dy);
    }
}
