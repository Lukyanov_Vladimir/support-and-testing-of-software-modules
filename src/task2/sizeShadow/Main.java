package task2.sizeShadow;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Snippet> listSnippets = new ArrayList<>();

        System.out.println("Введите количество отрезков:");
        int numSnippets = sc.nextInt();
        for (int i = 0; i < numSnippets; i++) {
            System.out.println("Введите отрезок №" + (i+1));
            System.out.println("Введите координаты первой точки отрезка через пробел:");
            Point2D point1 = new Point2D(sc.nextDouble(), sc.nextDouble());

            System.out.println("Введите координаты второй точки отрезка через пробел:");
            Point2D point2 = new Point2D(sc.nextDouble(), sc.nextDouble());

            Snippet snippet = new Snippet(point1, point2);

            listSnippets.add(snippet);
        }

        SizeShadow shadow = new SizeShadow();
        System.out.println(shadow.calcShadow(listSnippets));
    }
}
