package task3;

public class BranchingAlg {
    private double x;
    private double y;
    private int functionNum;

    public BranchingAlg(double x, double y, int functionNum) {
        this.x = x;
        this.y = y;
        this.functionNum = functionNum;
    }

    public double cos(double num) {
        return Math.cos(num);
    }

    public double pow(double num, double power) {
        return Math.pow(num, power);
    }

    public double exp(double num) {
        return Math.exp(num);
    }

    public double log(double num) {
        return Math.log(num);
    }

    public double abs(double num) {
        return Math.abs(num);
    }

    public double tan(double num) {
        return Math.tan(num);
    }

    public double sqrt(double num) {
        return Math.sqrt(num);
    }

    public double solve() {
        double answer;
        double resultFun = calcFun();

        if (x / y > 0)
            answer = log(y + 2) + resultFun;
        else if (x / y < 0)
            answer = log(abs(y)) - tan(resultFun);
        else
            answer = resultFun * pow(y, 3);

        return answer;
    }

    private double calcFun() {
        double resultFun;

        switch (functionNum) {
            case 1:
                resultFun = cos(x);
                break;

            case 2:
                resultFun = sqrt(x);
                break;

            case 3:
                resultFun = exp(x);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + functionNum);
        }

        return resultFun;
    }
}
