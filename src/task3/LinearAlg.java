package task3;

public class LinearAlg {

    private double x;
    private double y;
    private double z;

    public LinearAlg(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double cos(double num) {
        return Math.cos(num);
    }

    public double pow(double num, double power) {
        return Math.pow(num, power);
    }

    public double solve() {
        double PI = Math.PI;
        return ((2 * cos(x - PI / 6)) / (0.5 + (1 - cos(2 * y)) / 2)) * (1 + (pow(z, 2) / (3 - (pow(z, 2) / 5))));
    }
}
