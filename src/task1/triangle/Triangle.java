package task1.triangle;

public class Triangle {
    private final int firstSideLength;
    private final int secondSideLength;
    private final int thirdSideLength;

    public Triangle(int firstSideLength, int secondSideLength, int thirdSideLength) {
        this.firstSideLength = firstSideLength;
        this.secondSideLength = secondSideLength;
        this.thirdSideLength = thirdSideLength;
    }

    public String determineTypeTriangle() {
        String message;
        if (!checkExistenceTriangle()) {
            message = "Такой треугольник не существует, проверьте данные";

        } else if (checkIfEquilateralTriangle()) {
            message = "Треугольник является равносторонним";

        } else if (checkWhetherAnIsoscelesTriangle()) {
            message = "Треугольник является равнобедренным";

        } else {
            message = "Треугольник является неравносторонним";
        }

        return message;
    }

    public boolean checkExistenceTriangle() {
        return (firstSideLength + secondSideLength > thirdSideLength)
                && (firstSideLength + thirdSideLength > secondSideLength)
                && (secondSideLength + thirdSideLength > firstSideLength);
    }

    public boolean checkIfEquilateralTriangle() {
        return firstSideLength == secondSideLength && firstSideLength == thirdSideLength;
    }

    public boolean checkWhetherAnIsoscelesTriangle() {
        return firstSideLength == secondSideLength || firstSideLength == thirdSideLength || secondSideLength == thirdSideLength;

    }
}
