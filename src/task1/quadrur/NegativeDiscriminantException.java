package task1.quadrur;

public class NegativeDiscriminantException extends Exception {
    public NegativeDiscriminantException(String message) {
        super(message);
    }
}
