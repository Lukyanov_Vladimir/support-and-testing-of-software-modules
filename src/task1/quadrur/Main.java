package task1.quadrur;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);

            System.out.println("Введите действительное число №1");
            int firstRealNumber = sc.nextInt();

            System.out.println("Введите действительное число №2");
            int secondRealNumber = sc.nextInt();

            System.out.println("Введите действительное число №3 ");
            int thirdRealNumber = sc.nextInt();

            QuadraticEquation quadraticEquation = new QuadraticEquation(firstRealNumber, secondRealNumber, thirdRealNumber);

            double[] roots = quadraticEquation.findTheRoots();

            if (roots[0] == roots[1]) {
                System.out.println("Уравнение имеет один корень: x = " + roots[0]);
            } else {
                System.out.println("Уравнение имеет два корня: x1 = " + roots[0] + "; x2 = " + roots[1]);
            }

        } catch (InputMismatchException | NegativeDiscriminantException e) {
            System.err.println("Введены некорректные данные");
            e.printStackTrace();
        }
    }


}
